
public class Hydro extends Bill {
	private double unitConsumed;
	private String companyName;

	public Hydro(int billId, String billDate, double billAmount, double unitConsumed, String companyName) {

		super(billId, "Hydro", billDate, billAmount);
		this.unitConsumed = unitConsumed;
		this.companyName = companyName;

	}

	public double getUnitConsumed() {
		return unitConsumed;
	}

	public void setUnitConsumed(double unitConsumed) {
		this.unitConsumed = unitConsumed;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public void display() {
		super.display();
		System.out.println("Company Name: " + companyName);
		System.out.println("Units consumed: " + unitConsumed + " Units");

	}
}
