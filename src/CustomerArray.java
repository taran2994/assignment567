import java.util.ArrayList;
import java.util.Collections;

public class CustomerArray implements IDisplay {

	private ArrayList<Customer> customerList = new ArrayList<Customer>();

	public void getCustomer(int i) {
		customerList.get(i);

	}

	public void addCustomer(Customer c) {
		customerList.add(c);
	}

	public void sortCustomers() {
		Collections.sort(this.customerList);
	}

	@Override
	public void display() {

		sortCustomers();

		for (int i = 0; i < customerList.size(); i++) {
			Customer customerFirst = customerList.get(i);
			customerFirst.display();
		}

	}

	public Customer getCustomerById(int id) {

		for (int i = 0; i < customerList.size(); i++) {
			Customer cust1 = customerList.get(i);
			if (id == cust1.getCustomerId()) {
				return cust1;
			}

		}

		System.out.println("Customer with ID " + id + " does not exist");
		return null;

	}

}
