
public class Mobile extends Bill {
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public double getGbUsed() {
		return gbUsed;
	}

	public void setGbUsed(double gbUsed) {
		this.gbUsed = gbUsed;
	}

	public double getMinutesUsed() {
		return minutesUsed;
	}

	public void setMinutesUsed(double minutesUsed) {
		this.minutesUsed = minutesUsed;
	}

	private String mobileNumber;
	private String manufacturerName;
	private String planName;
	private double gbUsed;
	private double minutesUsed;

	public Mobile(int billId, String billDate, double billAmount, double gbUsed, String mobileNumber,
			String manufacturerName, String planName, double minutesUsed) {

		super(billId, "Mobile", billDate, billAmount);
		this.gbUsed = gbUsed;
		this.manufacturerName = manufacturerName;
		this.mobileNumber = mobileNumber;
		this.planName = planName;
		this.minutesUsed = minutesUsed;

	}

	@Override
	public void display() {
		super.display();
		System.out.println("Manufacturer Name: "+manufacturerName);
		System.out.println("Plan name: "+planName);
		System.out.println("Mobile number: "+mobileNumber);
		System.out.println("Internet usage: "+gbUsed+" GB");
		System.out.println("Minutes usage: "+minutesUsed+" minutes");
		
		
	}
}
