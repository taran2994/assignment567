
public class Bill implements IDisplay, Comparable<Bill> {
	private int billId;
	private String billType;
	private String billDate;
	private Double billAmount;

	public Bill(int billId, String billType, String billDate, double billAmount) {
		this.billId = billId;
		this.billType = billType;
		this.billDate = billDate;
		this.billAmount = billAmount;
	}

	public int getBillId() {
		return billId;
	}

	public void setBillId(int billId) {
		this.billId = billId;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public Double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	@Override
	public void display() {
		System.out.println("Bill Id: " + billId);
		System.out.println("Bill Date: " + billDate);
		System.out.println("Bill Type: " + billType);
		System.out.println("Bill Amount: $" + billAmount);

	}

	@Override
	public int compareTo(Bill bill2) {

		return this.getBillAmount().compareTo(bill2.getBillAmount());
	}

}
