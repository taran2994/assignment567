
public class Internet extends Bill {
	private double gbUsed;
	private String providerName;

	public Internet(int billId, String billDate, double billAmount, double gbUsed, String providerName) {

		super(billId, "Internet", billDate, billAmount);
		this.gbUsed = gbUsed;
		this.providerName = providerName;

	}

	public double getGbUsed() {
		return gbUsed;
	}

	public void setGbUsed(double gbUsed) {
		this.gbUsed = gbUsed;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	@Override
	public void display() {
		super.display();
		System.out.println("Provider Name: " + providerName);
		System.out.println("Internet Usage: " + gbUsed + " GB");

	}

}
