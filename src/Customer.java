import java.util.ArrayList;
import java.util.Collections;

public class Customer implements IDisplay, Comparable<Customer> {

	private Integer customerId;
	private String firstName;
	private String lastName;
	private String fullName;
	private String emailId;
	private double totalAmount;
	private ArrayList<Bill> billList = new ArrayList<Bill>();

	public void addBill(Bill b) {
		billList.add(b);
	}

	public void getBill(int i) {
		billList.get(i);

	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public ArrayList<Bill> getBillList() {
		return billList;
	}

	public void setBillList(ArrayList<Bill> billList) {
		this.billList = billList;
	}

	public Customer(int customerId, String firstName, String lastName, String emailId) {
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = firstName + " " + lastName;
		this.emailId = emailId;
	}

	public double getTotalBillAmount() {
		double sum = 0;

		for (int i = 0; i < billList.size(); i++) {
			Bill billFirst = billList.get(i);
			sum += billFirst.getBillAmount();

		}

		return sum;
	}

	public void sortBills() {
		Collections.sort(this.billList);
	}

	@Override
	public void display() {

		System.out.println("Customer ID : " + getCustomerId());
		System.out.println("Customer Full Name : " + getFullName());
		System.out.println("Customer Email ID : " + getEmailId());
		System.out.println();
		if (billList.size() == 0) {
			System.out.println("Note: This customer has no bills");

		} else {

			System.out.println("           -----Bill Information----- ");
			System.out.println("***********************************************");

			sortBills();
			for (int i = 0; i < billList.size(); i++) {
				Bill billFirst = billList.get(i);
				billFirst.display();
				System.out.println("***********************************************");

			}

			System.out.println("Total bill amount to Pay :$" + getTotalBillAmount());
			System.out.println("***********************************************");

		}
	}

	@Override
	public int compareTo(Customer cust1) {
		return this.getCustomerId().compareTo(cust1.getCustomerId());
	}

}
