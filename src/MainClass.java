
public class MainClass {
	public static void main(String[] args) {
		Bill bill1 = new Hydro(1, "02,Wednesday,2019", 45.00, 29, "Hydro One");
		Bill bill2 = new Internet(2, "02,Wednesday,2019", 56, 500, "Rogers");

		Customer customer1 = new Customer(1, "Taranpreet", "Singh", "taran2994@gmail.com");

		customer1.addBill(bill1);
		customer1.addBill(bill2);

		Bill bill3 = new Hydro(1, "03,Thursday,2019", 45.13, 29, "Enbridge");
		Bill bill4 = new Internet(2, "03,Thursday,2019", 56.50, 500, "Freedom");
		Bill bill5 = new Mobile(3, "04,Friday,2019", 250.65, 5, "906676654", "Samsung", "Prepaid talk+ internet plan",
				356);
		Bill bill6 = new Mobile(4, "04,Friday,2019", 300.78, 4, "906787654", "Apple", "Big gig plan", 230);

		Customer customer2 = new Customer(2, "Navpreet", "Kaur", "Navpreet230@gmail.com");

		customer2.addBill(bill3);
		customer2.addBill(bill4);
		customer2.addBill(bill5);
		customer2.addBill(bill6);

		Customer customer3 = new Customer(3, "Jagseer", "Kaur", "sranjagseer@gmail.com");

		CustomerArray cusArr = new CustomerArray();

		cusArr.addCustomer(customer1);
		cusArr.addCustomer(customer2);
		cusArr.addCustomer(customer3);

		cusArr.display();
	}
}
